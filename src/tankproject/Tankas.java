package tankproject;

import tankproject.entities.Tank;
import tankproject.enums.Direction;

public class Tankas {
	private Tank tank;
	
	public Tankas() {
		tank = new Tank();
		tank.setSpeedBackward(-1);
		tank.setSpeedForward(1);
	}
	
	public void desinen() {
		tank.move(Direction.RIGHT);
	}
	public void kairen() {
		tank.move(Direction.LEFT);
	}
	public void pirmyn() {
		tank.move(Direction.FORWARD);
	}
	public void atgal() {
		tank.move(Direction.BACKWARD);
	}
	public void suvis() {
		tank.shoot();
	}
	public void info() {
		System.out.println(tank.toString());
	}
	public void uztaisytiGinkla() {
		tank.addShells();
	}
	public void aplankytiTaskus(/*Taskas[] taskaiAplankyt*/) {}
	
}
