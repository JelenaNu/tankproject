package tankproject.entities;

import tankproject.accessories.Helper;
import tankproject.enums.WorldSide;
import static tankproject.accessories.StringsLT.*;

public class Shot {
	private Point point;
	private WorldSide worldSide;
	
	public Shot(Point point, WorldSide worldSide) {
		this.point = point;
		this.worldSide = worldSide;
	}
	
	public WorldSide getWorldSide() {
		return worldSide;
	}
	
	@Override
	public String toString() {
		return DIRECTION_STRING + " = " + Helper.getDirectionString(worldSide) +
				", " + POSITION_STRING +
				" (" + Helper.getPositionString(point) + ")";
	}
}
