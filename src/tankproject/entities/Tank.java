package tankproject.entities;

import tankproject.accessories.Helper;
import tankproject.enums.Direction;
import tankproject.enums.WorldSide;

import static tankproject.accessories.StringsLT.*;

public class Tank {
	
	private final int DEFAULT_SHELLCOUNT = 10;
	private final int DEFAULT_SPEED_FORWARD = 2;
	private final int DEFAULT_SPEED_BACKWARD = -1;
	
	private int speedForward;
	private int speedBackward;;
	private Point point;
	private WorldSide worldSide;
	private int shellCount;
	private Shot[] shots;
	private int shotCount;
	
	public Tank() {
		point = new Point(0, 0);
		worldSide = WorldSide.NORTH;
		speedBackward = DEFAULT_SPEED_BACKWARD;
		speedForward = DEFAULT_SPEED_FORWARD;
		shellCount = DEFAULT_SHELLCOUNT;
		shots = new Shot[DEFAULT_SHELLCOUNT];
		shotCount = 0;
	}
	
	private void setDefaultPosition() {
		point.setX(0);
		point.setY(0);
		worldSide = WorldSide.NORTH;
	}
	//TODO
	public String createRoute(Point[] points){
		setDefaultPosition();
		for (Point p : points) {
			
		}
		return null;
	}
	
	public void move(Direction direction) {
		
		if (direction == Direction.RIGHT || direction == Direction.LEFT) {
			turn(direction);
		}
		
		int speed;
		if (direction == Direction.BACKWARD){
			speed = speedBackward;
		} else {
			speed = speedForward;
		}
		
		switch (worldSide) {
			case NORTH:
				//y += speed;
				point.setY(point.getY() + speed);
				break;
			case NORTHEAST:
				//y += speed;
				point.setY(point.getY() + speed);
				//x += speed;
				point.setX(point.getX() + speed);
				break;
			case EAST:
				//x += speed;
				point.setX(point.getX() + speed);
				break;
			case SOUTHEAST:
				//x += speed;
				point.setX(point.getX() + speed);
				//y -= speed;
				point.setY(point.getY() - speed);
				break;
			case SOUTH:
				//y -= speed;
				point.setY(point.getY() - speed);
				break;
			case SOUTHWEST:
				//x -= speed;
				point.setX(point.getX() - speed);
				//y -= speed;
				point.setY(point.getY() - speed);
				break;
			case WEST:
				//x -= speed;
				point.setX(point.getX() - speed);
				break;
			case NORTHWEST:
				//x -= speed;
				point.setX(point.getX() - speed);
				//y += speed;
				point.setY(point.getY() + speed);
		}
	}
	
	private void turn(Direction direction) {
		if (direction == Direction.LEFT || direction == Direction.RIGHT) {
			changeWorldSide(direction);
		}
	}
	
	public void addShells(){
		// creating a new bigger array
		if (shellCount == 0) {
			Shot[] newArray = new Shot[shots.length + DEFAULT_SHELLCOUNT];
			System.arraycopy(shots, 0, newArray, 0, shots.length);
			shots = newArray;
			shellCount = DEFAULT_SHELLCOUNT;
		}
	}
	
	public int getShellCount() {
		return shellCount;
	}
	
	public void shoot() {
		if (shellCount != 0) {
			shots[shotCount] = new Shot(point, worldSide);
			shotCount++;
			shellCount--;
		}
	}
	// switches worldsides round the worldside elements array
	// for turning the tank left or right
	private void changeWorldSide(Direction direction) {
		WorldSide w;
		int currentWorldSideOrdinal = worldSide.ordinal();
		switch (direction) {
			case LEFT:
				if (currentWorldSideOrdinal == 0) {
					w = WorldSide.values()[WorldSide.values().length - 1];
				} else {
					w = WorldSide.values()[currentWorldSideOrdinal - 1];
				}
				break;
			case RIGHT:
				if (currentWorldSideOrdinal == WorldSide.values().length - 1) {
					w = WorldSide.values()[0];
				} else {
					w = WorldSide.values()[currentWorldSideOrdinal + 1];
				}
				break;
			default:
				w = WorldSide.NORTH;
		}
		setWorldSide(w);
	}
	
	private void setWorldSide(WorldSide worldSide) {
		this.worldSide = worldSide;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(TANKPOSITION_STRING).append(": ").append(Helper.getPositionString(point));
		sb.append("\n");
		sb.append(TANKDIRECTION_STRING).append(": ").append(Helper.getDirectionString(worldSide));
		sb.append("\n");
		sb.append(TANKSHOTS_STRING).append(": ").append(shotCount);
		sb.append("\n");
		sb.append(worldSidesShotsString());
		for (int i = 0; i < shots.length; i++) {
			if (shots[i] == null) {
				break;
			}
			sb.append(TANKSHOT_NUM_STRING).append(" ").append(i + 1).append(": ").append(shots[i]).append("\n");
		}
		return sb.toString();
	}
	
	private String worldSidesShotsString(){
		StringBuilder sb = new StringBuilder();
		int[] worldSideShotCount = new int[WorldSide.values().length];
		for (Shot shot : shots) {
			if (shot == null) {
				break;
			}
			worldSideShotCount[shot.getWorldSide().ordinal()]++;         //  counting how many shots to the concrete
		}                                                               //  world side; the i index is ordinal in enum
		for (int i = 0; i < worldSideShotCount.length; i++) {
			if (worldSideShotCount[i] > 0) {
				sb.append(TANKSHOTS_STRING).append(": ");
				sb.append(Helper.getDirectionString(WorldSide.values()[i]));
				sb.append(": ").append(worldSideShotCount[i]);
				sb.append("\n");
			}
		}
		return sb.toString();
	}
	
	public void setSpeedForward(int speedForward) {
		this.speedForward = speedForward;
	}
	
	public void setSpeedBackward(int speedBackward) {
		this.speedBackward = speedBackward;
	}
}
