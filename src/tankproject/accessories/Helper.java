package tankproject.accessories;

import tankproject.entities.Point;
import tankproject.enums.WorldSide;

import static tankproject.accessories.StringsLT.*;

public class Helper {
	
	public static String getDirectionString(WorldSide worldSide) {
		String s = "";
		switch (worldSide) {
			case NORTH:
				s = NORTH_STRING;
				break;
			case NORTHEAST:
				s = NORTH_STRING + " " + EAST_STRING;
				break;
			case EAST:
				s = EAST_STRING;
				break;
			case SOUTHEAST:
				s = SOUTH_STRING + " " + EAST_STRING;
				break;
			case SOUTH:
				s = SOUTH_STRING;
				break;
			case SOUTHWEST:
				s = SOUTH_STRING + " " + WEST_STRING;
				break;
			case WEST:
				s = WEST_STRING;
				break;
			case NORTHWEST:
				s = NORTH_STRING + " " + WEST_STRING;
		}
		return s;
	}
	
	public static String getPositionString(Point point) {
		// create worldside positions
		StringBuilder sb = new StringBuilder();
		if (!describeX(point.getX()).isEmpty()) {
			sb.append(describeX(point.getX())).append(": ").append(Math.abs(point.getX()));
		}
		if (!describeY(point.getY()).isEmpty()) {
			if (!sb.toString().isEmpty()) {
				sb.append(", ");
			}
			sb.append(describeY(point.getY())).append(": ").append(Math.abs(point.getY()));
		}
		if (sb.toString().isEmpty()) {
			sb.append(0);
		}
		return sb.toString();
	}
	
	private static String describeX(int x) {
		String description = "";
		if (x < 0) {
			description = WEST_STRING;
		}
		if (x > 0) {
			description = EAST_STRING;
		}
		return description;
	}
	
	private static String describeY(int y) {
		String description = "";
		if (y < 0) {
			description = SOUTH_STRING;
		}
		if (y > 0) {
			description = NORTH_STRING;
		}
		return description;
	}
	
	private Helper() {
	}
}
