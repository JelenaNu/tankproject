package tankproject.accessories;

public class StringsLT {
	
	public static final String PRESSKEY_STRING = "Paspauskite migtuką";
	public static final String SHOWINFO_STRING = "parodyti informaciją";
	public static final String EXIT_STRING = "baigti programą";
	public static final String FORWARD_STRING = "pirmyn";
	public static final String BACKWARD_STRING = "atgal";
	public static final String LEFT_STRING = "kairėn";
	public static final String RIGHT_STRING = "dešinėn";
	public static final String SHOOT_STRING = "šauti";
	public static final String NOSHELLS_STRING = "truksta sviedinių";
	public static final String LEFTSHELLS_STRING = "liko";
	public static final String WRONGCOMMAND_STRING = "neteisinga komanda";
	public static final String ADDSHELLS_STRING = "užtaisyti ginklą";
	public static final String ENOUGHSHELLS_STRING = "sviedinių pakanka";
	public static final String NORTH_STRING = "Šiaurė";
	public static final String EAST_STRING = "Rytai";
	public static final String SOUTH_STRING = "Pietūs";
	public static final String WEST_STRING = "Vakarai";
	public static final String TANKPOSITION_STRING = "Tanko pozicija";
	public static final String TANKDIRECTION_STRING = "Tanko kryptis";
	public static final String TANKSHOTS_STRING = "Tanko iššauti šoviniai";
	public static final String TANKSHOT_NUM_STRING = "Tanko šūvis nr.";
	public static final String DIRECTION_STRING = "kryptis";
	public static final String POSITION_STRING = "pozicija";
	public static final String SPACE_STRING = "Tarpas";
	public static final String SHOW_ROUTE_STRING = "parodyti maršrutą pagal įvestus taškus";
	public static final String READ_POINT_PATTERN_STRING = "([VvRr][1-9]{1,}[ŠšPp][1-9]{1,})"; // english ([WwEe][1-9]{1,}[NnSs][1-9]{1,})
	public static final String ENTER_POINT_COUNT_STRING = "įveskite taškų kiekį";
	public static final String POINT_NUMBER_STRING = "taškas #";
	public static final String CANCEL_STRING = "atšaukti";
	public static final String PATTERN_TO_ENTER_STRING = "įveskite tašką pagal šabloną";
	public static final String PATTERN_STRING = "V2P4";
	public static final String PATTERN_WORDLSIDES_STRING = "V - " + WEST_STRING + " R - " + EAST_STRING +
			" Š - " + NORTH_STRING + " P - " + SOUTH_STRING;
	public static final String OR_PRESS_STRING = "arba paspauskite";
	
	
	
	private StringsLT() {
	}
}
