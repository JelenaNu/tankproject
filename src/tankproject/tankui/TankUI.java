package tankproject.tankui;

import tankproject.entities.Point;
import tankproject.entities.Tank;
import tankproject.enums.Command;
import tankproject.enums.Direction;

import java.util.Arrays;
import java.util.Scanner;

import static tankproject.accessories.StringsLT.*;

public class TankUI {
	
	private Scanner scanner = new Scanner(System.in);
	
	private Tank tank;
	private final String[] menuItems = {
			PRESSKEY_STRING + ": ",
			Command.MOVE_FORWARD.getDescription() + " - " + FORWARD_STRING,
			Command.MOVE_BACKWARD.getDescription() + " - " + BACKWARD_STRING,
			Command.MOVE_LEFT.getDescription() + " - " + LEFT_STRING,
			Command.MOVE_RIGHT.getDescription() + " - " + RIGHT_STRING,
			Command.SHOOT.getDescription() + " - " + SHOOT_STRING,
			Command.ADD_SHELLS.getDescription() + " - " + ADDSHELLS_STRING,
			Command.SHOW_INFO.getDescription() + " - " + SHOWINFO_STRING,
			Command.MAKE_ROUTE_FOR_POINTS.getDescription() + " - " + SHOW_ROUTE_STRING,
			Command.EXIT.getDescription() + " - " + EXIT_STRING
	};
	
	public static void main(String[] args) {
		TankUI tankUI = new TankUI();
		tankUI.tank = new Tank();
		tankUI.menu();
	}
	
	private void menu(){
		while (true) {
			for (String item : menuItems) {
				print(item);
			}
			String command = scanner.nextLine();
			commandExecute(command);
		}
	}
	private void commandExecute(String command) {
		if (command.equalsIgnoreCase(Command.MOVE_FORWARD.getKey())) {
			executeMove(FORWARD_STRING, Direction.FORWARD);
		} else if (command.equalsIgnoreCase(Command.MOVE_BACKWARD.getKey())) {
			executeMove(BACKWARD_STRING, Direction.BACKWARD);
		} else if (command.equalsIgnoreCase(Command.MOVE_LEFT.getKey())) {
			executeMove(LEFT_STRING, Direction.LEFT);
		} else if (command.equalsIgnoreCase(Command.MOVE_RIGHT.getKey())) {
			executeMove(RIGHT_STRING, Direction.RIGHT);
		} else if (command.equalsIgnoreCase(Command.SHOOT.getKey())) {
			executeShoot();
		} else if (command.equalsIgnoreCase(Command.ADD_SHELLS.getKey())) {
			executeAddShells();
		} else if (command.equalsIgnoreCase(Command.SHOW_INFO.getKey())) {
			printInfo();
		} else if (command.equalsIgnoreCase(Command.MAKE_ROUTE_FOR_POINTS.getKey())) {
			executeMakeRoute();
		} else if (command.equalsIgnoreCase(Command.EXIT.getKey())) {
			System.exit(0);
		} else {
			print(WRONGCOMMAND_STRING);
		}
	}
	
	private void executeMakeRoute(){
		print(SHOW_ROUTE_STRING);
		
		int pointCount;
		Point[] points = null;
		
		print(ENTER_POINT_COUNT_STRING);
		try {
			pointCount = scanner.nextInt();
			String coordinate = scanner.nextLine();
			points = new Point[pointCount];
			
			print(PATTERN_TO_ENTER_STRING + " " + PATTERN_STRING);
			print(OR_PRESS_STRING + " " + Command.EXIT.getKey() + " " + CANCEL_STRING);
			print(PATTERN_WORDLSIDES_STRING);
			
			int i = 0;
			do {
				print(POINT_NUMBER_STRING + (i + 1));
				coordinate = scanner.nextLine();
				if (coordinate.equalsIgnoreCase(Command.EXIT.getKey())) {
					print(CANCEL_STRING);
					break;
				}
				if (isCoordinateCorrect(coordinate)) {
					points[i] = getPointByCoordinate(coordinate);
					i++;
					if (i == pointCount) {
						break;
					}
				} else {
					print(WRONGCOMMAND_STRING);
				}
			} while (true);
		} catch (Exception e) {
			print(WRONGCOMMAND_STRING);
		}
	
		if (points != null) {
			executeCreateRoute(points);
		}
		
	}
	
	void executeCreateRoute(Point[] points) {
		print(tank.createRoute(points));
	}
	
	private Point getPointByCoordinate(String coordinate) {
		char[] possibleCoordinateChars = {'V', 'R', 'Š', 'P'};
		char[] coordinateChars = coordinate.toUpperCase().toCharArray();
		
		int kx = 1, ky = 1;
		if (coordinateChars[0] == possibleCoordinateChars[0]) {
			kx = -1; // will set X direction left (West)
		}
		int i = 1;
		while (Character.isDigit(coordinateChars[i])) {
			i++;
		}
		int x = Integer.parseInt(coordinate.substring(1, i)) * kx;
		if (coordinateChars[i] == possibleCoordinateChars[3]) {
			ky = -1; // will set Y direction down (South)
		}
		int y = Integer.parseInt(coordinate.substring(i + 1, coordinate.length())) * ky;
		return new Point(x, y);
	}
	
	private boolean isCoordinateCorrect(String coordinate) {
		return coordinate.matches(READ_POINT_PATTERN_STRING);
	}
	
	private void print(String text) {
		System.out.println(text);
	}
	
	private void executeMove(String text, Direction direction) {
		print(text);
		tank.move(direction);
	}
	
	private void executeShoot() {
		print(SHOOT_STRING);
		if (tank.getShellCount() == 0) {
			print(NOSHELLS_STRING);
		} else {
			tank.shoot();
			print(LEFTSHELLS_STRING + " " + tank.getShellCount());
		}
	}
	
	private void executeAddShells() {
		print(ADDSHELLS_STRING);
		if (tank.getShellCount() == 0) {
			tank.addShells();
		} else {
			print(ENOUGHSHELLS_STRING);
		}
	}
	
	private void printInfo() {
		print(tank.toString());
	}
	
}
