package tankproject.enums;

public enum Direction {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
}
