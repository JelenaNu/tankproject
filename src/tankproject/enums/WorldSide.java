package tankproject.enums;

public enum WorldSide {
	NORTH, // UP  x++ y
	NORTHEAST, // UP RIGHT x++ y++
	EAST, // RIGHT x y++
	SOUTHEAST, // RIGHT DOWN x++ y--
	SOUTH, // DOWN x y--
	SOUTHWEST, // LEFT DOWN x-- y--
	WEST, // LEFT x-- y
	NORTHWEST // LEFT UP x-- y++;
}
