package tankproject.enums;

import tankproject.accessories.StringsLT;

public enum Command {
	MOVE_FORWARD("W", "W"),
	MOVE_BACKWARD("S", "S"),
	MOVE_LEFT("A", "A"),
	MOVE_RIGHT("D", "D"),
	SHOOT(" ", StringsLT.SPACE_STRING),
	ADD_SHELLS("+", "+"),
	SHOW_INFO("I", "I"),
	MAKE_ROUTE_FOR_POINTS("R", "R"),
	EXIT("X", "X");
	
	private String key;
	private String description;
	
	Command(String key, String description) {
		this.key = key;
		this.description = description;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getDescription() {
		return description;
	}
}
